package com.globaldairytrade;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import java.util.List;

import com.globaldairytrade.bizservices.identity.v1x0.resource.UsersService;
import com.globaldairytrade.userrole.CSVReader;
import com.globaldairytrade.userrole.UserBean;
import com.globaldairytrade.userrole.UserRoleUpdater;
import org.apache.commons.lang.StringUtils;
import org.glassfish.jersey.client.proxy.WebResourceFactory;

public class Application {

	/**
	 * Run with arguments:
	 * -Psrc/main/resources/manipulator.properties -Dlog4j.configurationFile=src/main/resources/log4j2.xml --csv="/home/chris/Work/GDT/gdt-user-role-manipulator/UsersUpdate.csv"
	 */
	public static void main(String[] args) {
		String csvFile = null;
		for (String arg : args) {
			if (arg.startsWith("--csv")) {
				csvFile = arg.split("=")[1];
			}
		}

		if (StringUtils.isBlank(csvFile)) {
			throw new RuntimeException("The --csv argument needs to be provided");
		}

		Client client = ClientBuilder.newBuilder().build();
		WebTarget target = client.target("http://localhost:9090/identity/1/0/");
		UsersService usersService = WebResourceFactory.newResource(UsersService.class, target);
		UserRoleUpdater userRoleUpdater = new UserRoleUpdater(usersService);

		List<UserBean> userBeanList = CSVReader.readFile(csvFile);
		userRoleUpdater.updateUserRoles(userBeanList, new String[]{"TSEM_BIDDER", "PORTAL_USER_ADMIN"});
	}
}

