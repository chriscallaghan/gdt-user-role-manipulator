package com.globaldairytrade.userrole

import com.opencsv.bean.CsvBindByName

class UserBean {

	@CsvBindByName(column = "Bidder number")
	String bidderNumber
	@CsvBindByName(column = "Bidder name")
	String bidderName
	@CsvBindByName(column = "First name")
	String firstName
	@CsvBindByName(column = "Last name")
	String lastName
	@CsvBindByName(column = "Contact name")
	String contactName
	@CsvBindByName(column = "Phone number")
	String phoneNumber
	@CsvBindByName(column = "Alternative phone number")
	String alternativePhoneNumber
	@CsvBindByName(column = "Country")
	String country
	@CsvBindByName(column = "Region")
	String region
	@CsvBindByName(column = "MyGDT Status")
	String status

}
