package com.globaldairytrade.userrole

import com.globaldairytrade.bizservices.identity.v1x0.resource.UsersService
import com.globaldairytrade.common.services.v1x0.WellKnownRoleName
import com.globaldairytrade.common.services.v1x0.dto.UserDetails
import com.globaldairytrade.common.services.v1x0.dto.UserDetailsList
import com.globaldairytrade.common.services.v1x0.dto.UserRolesUpdateRequest
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import javax.ws.rs.core.Response
import java.util.stream.Collectors
import java.util.stream.Stream

class UserRoleUpdater {

	private static final Logger logger = LoggerFactory.getLogger(UserRoleUpdater.class)
	private UsersService usersService

	UserRoleUpdater(UsersService usersService) {
		this.usersService = usersService
	}

	void updateUserRoles(List<UserBean> userBeans, String[] roles) {
		int updateCount = 0
		List<String> errorUsers = []
		List<String> notFoundUsers = []

		List<WellKnownRoleName> roleNames = Stream.of(roles)
				.map({ name -> WellKnownRoleName.valueOf(name) })
				.collect(Collectors.toList())

		userBeans.forEach({ bean ->
			UserDetails user = getUserDetails(bean)
			if (user) {
				UserRolesUpdateRequest request = new UserRolesUpdateRequest()
				request.roleNames = roleNames
				request.retainExistingRoles = true
				request.performedByGDTUserAdmin = true

				Response response = usersService.addRolesToUser(user.id, request)
				if (response.status == 200) {
					updateCount++
				} else {
					logger.error("Failed to update user {}. Received http status {}", bean.contactName, response.status)
					errorUsers << bean.contactName
				}
			} else {
				logger.warn("Unable to find user {}", bean.contactName)
				notFoundUsers << bean.contactName
			}
		})
		logger.info("Role update complete. {} users updated, {} users could not be found and there were {} errors", updateCount, notFoundUsers.size(), errorUsers.size())
		if (!notFoundUsers.isEmpty()) {
			logger.info("Not found users:\n{}", notFoundUsers.join(",\n"))
		}
		if (!errorUsers.isEmpty()) {
			logger.info("Error users:\n{}", errorUsers.join(",\n"))
		}
	}

	private UserDetails getUserDetails(UserBean userBean) {
		UserDetailsList userDetailsList = usersService.searchUsers(userBean.contactName, null, null)
		if (userDetailsList.userDetailsList.isEmpty()) {
			logger.debug("Not found using contact name, trying 'firstname lastname'")
			userDetailsList = usersService.searchUsers(userBean.firstName + " " + userBean.lastName, null, null)
			if (userDetailsList.userDetailsList.isEmpty()) {
				logger.debug("Not found using 'firstname lastname'")
			} else {
				logger.debug("Found!")
			}
		}

		if (!userDetailsList.userDetailsList.isEmpty()) {
			for (UserDetails userDetails : userDetailsList.userDetailsList) {
				if (userDetails.firstname == userBean.firstName && userDetails.lastname == userBean.lastName) {
					return userDetails
				}
			}
		}

		return null
	}

}
