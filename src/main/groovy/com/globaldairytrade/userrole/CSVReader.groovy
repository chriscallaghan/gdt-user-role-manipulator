package com.globaldairytrade.userrole

import com.opencsv.bean.CsvToBeanBuilder

class CSVReader {

	static List<UserBean> readFile(String filename) {
		return new CsvToBeanBuilder(new FileReader(filename)).withType(UserBean.class).build().parse();
	}

}
